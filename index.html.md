---
title: B2B2C API Reference

language_tabs : # must be one of https://git.io/vQNgJ
 - javascript

toc_footers:
    - <a href='#'>Sign Up for a Developer Key</a>
    - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a> 

includes:
    - errors

search: true

code_clipboard: true

---

![alt text](/images/general-68af2602.png "Tune Protect logo")


# B2B2C API Documentation



# Introduction

Welcome to the new API integration documentation that is designed to help you integrate seamlessly to our platform.

You may self-signup to our UAT environment via [apiportaluat.tuneprotect.com](https://apiportaluat.tuneprotect.com). Once you have signed up, reach out to us so that we can approve your account. You may then create an Application and correcponding API Key from the self-service developer portal.

Our integrations team are at hand to support you along the way.

# About the API

This is a revamp version of previous B2B2C API that has been improved in terms of performance and also API specification.

# Suggested Development Process

* Create UAT Application and API Key from our developer portal.
* Using the Signature Header and Data Structure as found in this guide, write your code and test using the UAT Endpoint. Here are a couple of hints:
    * You can use the json data from this guide as a content starting point and send transactions from within Postman.
    * Using Postman you can input your data to create content and validate
    * Each API call requires a unique vendor reference ID
* Once the integration work is tested and approved, you will need to swap the UAT Endpoint and UAT App ID / API Key with the Live Production Platform Endpoint and App ID / API Key. Please Note: UAT and production credentials are NOT the same and production credentials are provided by the Protect Group team once tests have been validated.
* Remember that transactions sent through UAT will be not be officially protected and are not billed for, therefore you can send through as many as are needed for testing. We recommend a minimum of 10 test transactions before proceeding to the Live Production Platform.

# Project Details

## Scope

Scope of this document covers the specification and technical details in order partners to integrate with the new API.

The integration involves the criteria as below:

* Fetch access token for every API calls
    * Provide API to get access token using the shared credentials.

* Renew Token
    * Provide API to renew token if the token has been expired.

* Get Basic Quote Details
    * Provide API for partner to get basic quotation.

* API to Retrieve Back the Quotation
    * Provide API for partner to retrieve back the quotation based on quotation no

* API to Update Quote
    * Provide API for partner to update the quote.

* API for Submit Quotation
    * Provide API for partner to submit final quotation.

## Deliverables
Upon completion of the integration, partner will be able to purchase motor products seamlessly through this API.

# Solution View

## Logical Architecture

![](/images/overview-system.png)

Partners request TAOS B2B2C API Functionality as below:
* Get Basic Quotation
* Get Basic With Quotation No
* Update Quotation
* Confirm Quotation
* Resend Email

# Interface List
This section describes about all services that is part of Tune Protect scope that is exposed to partners to invoke.

|No| API | Protocol | Peer to Peer |
|---|----|----------|--------------|
|1|Get Basic Quotation|REST|Partners -> B2B2C API|
|2|Get Basic With Quotation No|REST|Partners -> B2B2C API|
|3|Update Quotation|REST|Partners -> B2B2C API|
|4|Confirm Quotation|REST|Partnes -> B2B2C API|
|5|Resend Email|REST|Partnes -> B2B2C API|

# E2E Service Flow

## Get Basic Quotation

|![](/images/GetBasicQuotation-1.png)|
|:---:|
| E2E Normal Flow for GetBasicQuotation API|

|![](/images/GetBasicQuotation-2.png)|
|:---:|
| E2E Flow for GetBasicQuotation API with NVIC List|

|![](/images/GetBasicQuotation-3.png)|
|:---:|
| E2E Flow for GetBasicQuotation with Selected NVIC List|

|Step|Description|API Reference|Impact/Dependency|
|----|-----------|-------------|-----------------|
| |GetBasicQuotation|getBasicQuotation| N/A|
| |Get VIX| Internal Request | N/A |
| |Get NCD| Internal Request | N/A|
| |Get Basic Premium Amount |Internal Request | N/A |

## Get Basic Quotation With Quotation No

|![](/images/GetBasicQuotationWithQuotationNo-1.png)|
|:---:|
| E2E Flow for Get Basic Quotation with QuotationNo|

|Step|Description|API Reference|Impact/Dependency|
|----|-----------|-------------|-----------------|
| |GetBasicQuotationViaQuotationNo|| N/A|

## Post Update Quotation

|![](/images/PostUpdateQuotation-1.png)|
|:---:|
| E2E Flow for Post Update Quotation|

|Step|Description|API Reference|Impact/Dependency|
|----|-----------|-------------|-----------------|
| |PostUpdateQuotation|| N/A|
| |GetBasicPremiumAmount|Internal Request|N/A|

## Post Confirm Quotation

|![](/images/PostConfirmQuotation-1.png)|
|:---:|
| E2E Flow for Post Confirm Quotation|

|Step|Description|API Reference|Impact/Dependency|
|----|-----------|-------------|-----------------|
| |PostConfirmQuotation|| N/A|

## Callback API to Partner

|![](/images/CallbackAPItoPartner-1.png)|
|:---:|
| E2E Flow for Callback API to Partner|

* B2B2C API will have a scheduler to checks every minute to retrieve the JPJ status of particular covernote number.

* Once the covernote gets updated from JPJ, an internal request will be trigger, and B2B2C API will fire to the return URL that is being supplied from the previous API call.

# Service URL

Refer to the following for complete service URL.

|Reference|Filtering|
|---------|---------|
|https://apiuat.tuneprotect.com/token||

#### Step 1 : Visit to the API portal via following URL [here](https://apiportaluat.tuneprotect.com) , and proceed into the Developer Portal. 

![](/images/token-tutorial-1.png)

#### Step 2 : Click "Login" on top right corner of the developer portal.

![](/images/token-tutorial-2.png)

#### Step 3 : Fill in your login credentials in the text box located at the center of the page and click "Continue" button located at the bottom right corner of the login page.

![](/images/token-tutorial-3.png)

#### Step 4 : After login, select "Application" located at the top banner of the page, then select "ADD NEW APPLICATION" located below "APPLICATIONS"

![](/images/token-tutorial-4.png)

#### Step 5 : Name the application under "Application Name" text box, and select your per token quota. Per Token Quota will set the restriction of the number of requests can be use under the same token.

![](/images/token-tutorial-5.png)

#### Step 6 : After created, click into your newly created application by clicking the name.

![](/images/token-tutorial-6.png)

#### Step 7 : Click on sandbox keys located at the left menu bar, copy down the token endpoint, consumer key and consumer secret

![](/images/token-tutorial-7.png)

#### Step 8 : In Postman, under authorization tab, 
1. select "OAuth2.0" as the type,
2. select "Request headers" as the add authorization data to
3. fill in "Bearer" as the header prefix

![](/images/token-tutorial-8.png)

#### Step 9 : Scroll down the authorization details
1. Select "Client Credentials" as Grant Type
2. Fill in the Token Endpoint copied from API Portal into the text box for "Access Token URL"
3. Fill in the Consumer Key copied from API Portal into text box for "Client ID"
4. Fill in the Consumer Secret copied from API Portal into textbox for "Client secret"

![](/images/token-tutorial-9.png)

#### Step 10 : Click "Get New Access Token" located at the bottom of the Authorization detail page

![](/images/token-tutorial-10.png)

#### Step 11 : Wait for a moment, "Manage Access Token" windows will be prompt out, click on the "Use Token" button

![](/images/token-tutorial-11.png)

#### Step 12 : Click on "Headers" tab, there should have the Authorization value assigned from the token you have generated just now.

![](/images/token-tutorial-12.png)

#### Please make sure Bearer Authorization token are ready in headers Authorization everytime when you trying to send request to WSO2 API.

# Integration Specification

this section describes Tune Protect integration specification in details which channel partners will follow to integrate with B2B2C API.

## API URL

Refer to the following for complete service URL

|Reference|Filtering|
|---------|---------|
|/partner/v1/||

## Get Basic Quotation

#### API Specification

|API Name|GetBasicQuotation|
|---|---|
|URL Path|/motor/PostGetBasicQuotation|
|Consuming System| Channel Partners|
|Feature|To acquire premium amount requestd|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/POST|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|

#### Request Header

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|Content-Type|String|M|
|Authorization|String|M|

#### Request Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|agentCode|String|M|
|vehRegNo|String|M|
|PolicyHolderBasicInfo|[Policy Holder Basic Info Model]|M|
|vehicleLocation|Integer|M|
|vehicleCoverage|Integer|O|
|vehicleCondition|Char|O|
|coverFrom|Date|M|
|coverTo|Date|M|
|sumInsured|Decimal|O|
|nvic|String|C|
|noOfDriver|Integer|O|


#### Response Body

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|statusCode|String|M|
|statusMessage|String|M|
|quotationNo|String|M|
|quotationStatusId|Integer|M|
|CoverFrom|Date|M|
|CoverTo|Date|M|
|customerVehicleInfo|[Vehicle Info Model]|M|
|listPlan|List [[Plan Model]]|C|
|nvicList|List [[NVIC Variant Model]]|C|

``` javascript
Example Request and Response Message

Request Header:
    {
        Content-Type: "application/json"
        Authorization: "Bearer eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJ0YW9zX3VhdEBjYXJib24uc3VwZXIiLCJhdXQiOiJBUFBMSUNBVElPTiIsImF1ZCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJuYmYiOjE2NDc0ODg1MzUsImF6cCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJzY29wZSI6ImRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvYXBpcG9ydGFsdWF0LnR1bmVwcm90ZWN0LmNvbTo0NDNcL29hdXRoMlwvdG9rZW4iLCJleHAiOjE2NDc0OTIxMzUsImlhdCI6MTY0NzQ4ODUzNSwianRpIjoiNzM4ZDU4YmUtMTk3Mi00M2YwLTk4YjItNTg1NDZlNjIzY2U5In0.dlZXiEZHwvQIeyk2JE52nEIDNZR6G54IVP5_WpfBjW57ZlUcGW1ZPT--6V5h9az5pPvwx-Sgev5sLDIHPQbem97kuOCWbc6X1Ki35_yHGHH8livXKdn9F6IOrwKrlrhu6ACV35OFDON1uZLA4ngnTGgtG1PFEErS8tm43JeOydUWj3rzfJQXqvH7DnITZO7Cle_tnkYnddwWstxVDRNFfY03fr55l6oC9BYWBq-tcBnfEvU5t0R-8_Oyb0s9PjqL_MOMSKUv9s7ppA1AeZ6A9xOUfbeAsQ7QV0KrkAvzyslCEqr-08wEkxTvADhknN0dmx_btmXMGclzcZVpQR9cPA"
    }

Example 1: getBasicQuotation returns sucessful response

Request:
    {
        "vehRegNo": "SAA3313J",
        "idNo": "660812-12-5057",
        "insuredType": "I",
        "insuredMaritalStatus": "M",
        "vehicleLocation": 12,
        "disabled": "N",
        "vehicleCoverage": 1,
        "vehicleCondition": "U",
        "coverFrom": "2022/02/10",
        "coverTo": "2023/02/09",
        "nvic": "",
        "agentCode": "011099-00"
    }

Response:
{
	"statusCode": "000000",
	"statusMessage": "Success",
	"customerVehicleInfo": {
		"builtType": "C.B.U.",
		"capacity": "2442",
		"chassisNo": "MMBJYKL10JH038811",
		"coverType": "3",
		"engineNo": "4N15UCT9337",
		"polExpDate": "30082022",
		"preInsCode": "408",
		"vehClass": "02",
		"vehMake": "25",
		"vehModel": "28",
		"vehRegNo": "SY8144",
		"vehUse": "1",
		"yearMake": "2018",
		"curNCD": "0.3833",
		"nextNCD": "0.45",
		"ncdEffDate": "31082021",
		"ncdExpDate": "30082022",
		"nextNcdEffDate": "31082022",
		"nvicData": {
			"nvic": "JIQ18A",
			"makeCode": "25",
			"make": "MITSUBISHI",
			"modelCode": "28",
			"model": "TRITON",
			"variant": "VGT A/T PREMIUM MY16 5 SP AUTOMATIC CONVENTIONAL",
			"category": "LCV",
			"seatingCapacity": "5",
			"engineCapacity": "2442",
			"marketValue": 72900.0,
			"yearMake": "2018"
		}
	},
	"nvicList": null,
	"listPlan": [
		{
			"planCode": "Basic",
			"currency": "MYR",
			"displayName": "Option 1",
			"quotedAmount": 1175.27,
			"sumInsured": 65610.0,
			"basePremium": 1099.31,
			"detailedQuotation": {
				"coverPremium": 0.0,
				"allRiderPremium": 0.0,
				"loading": 0.0,
				"basicPremium": 1998.74,
				"ncd": 899.43,
				"extendPremium": 0.0,
				"totalExtraBenefitPremium": 0.0,
				"premium": 0.0,
				"gstDiscountAmount": 0.0,
				"gstPerc": 0.0,
				"gstAmount": 0.0,
				"stampDuty": 10.0,
				"totalNetPremium": 1065.34,
				"totalAmountPayable": 1175.27,
				"schedulePremium": 0.0,
				"discountPercents": 0.0,
				"totalDiscountAmount": 0.0,
				"premiumAfterDiscount": 1099.31,
				"trailerPremium": 0.0,
				"totalExtraCoverPremium": 0.0,
				"noClaimDiscount": 0.0,
				"totalRidersPremium": 0.0,
				"grossPremium": 1099.31,
				"grossDue": 1175.27,
				"commisionPerc": 0.1,
				"commisionAmount": 109.93,
				"totalCommision": 109.93,
				"commisionGSTPerc": 0.0,
				"commisionGSTAmount": 0.0,
				"act": 419.94,
				"excessAmount": 656.0,
				"netBNMRound": 0.0,
				"sstPerc": 0.06,
				"sstAmount": 65.96,
				"refno": "20220308183731304PM",
				"verkey": "VK00013",
				"tecH_PREM": 2989.86,
				"t_BASICPREM_SR": 2220.83,
				"annualprem": 1856.34,
				"srate": 1.0767123287671233
			},
			"listExtraBenefit": [
				{
					"extensionData": null,
					"selected": false,
					"cewCode": "119",
					"baseCEWCode": "119",
					"resultRange": null,
					"sumInsured": 0.0,
					"unitValue": 0,
					"premium": 0.0,
					"formulaCode": "U",
					"unitName": "",
					"description": "All Drivers (Non-Tariff) (RM 20.00 per vehicle)",
					"openEditFor": "",
					"defaultUnitValue": 1,
					"defaultSumInsured": 0.0,
					"category": "BASIC",
					"planName": "",
					"stampDuty": 0.0
				},
				{
					"extensionData": null,
					"selected": false,
					"cewCode": "118",
					"baseCEWCode": "118",
					"resultRange": null,
					"sumInsured": 0.0,
					"unitValue": 0,
					"premium": 0.0,
					"formulaCode": "X",
					"unitName": "",
					"description": "Private Hire Car (100% of vehicle Sum Insured)",
					"openEditFor": "",
					"defaultUnitValue": 0,
					"defaultSumInsured": 0.0,
					"category": "BASIC",
					"planName": "",
					"stampDuty": 0.0
				}
			]
		}
	]
}

Example 2: getBasicQuotation returns with multiple NVIC list. (Partners need to resend back with selected nvic code - refer to Example 4)

Request : 
    {
        "vehRegNo": "SAA3313J",
        "idNo": "660812-12-5057",
        "insuredType": "I",
        "insuredMaritalStatus": "M",
        "vehicleLocation": 12,
        "disabled": "N",
        "vehicleCoverage": 1,
        "vehicleCondition": "U",
        "coverFrom": "2022/02/10",
        "coverTo": "2023/02/09",
        "nvic": "",
        "agentCode": "011099-00"
    }

Response :
    {
    "statusCode": "400011",
    "statusMessage": "Please select a NVIC code",
    "customerVehicleInfo": null,
    "listPlan": null,
    "nvicList": [
        {
            "nvic": "GOS05G",
            "variant": "G KUN25R-PRPSHE 4 SP AUTOMATIC"
        },
        {
            "nvic": "H6A05A",
            "variant": "SR TURBO KDN165R-PRMDYE 5 SP MANUAL"
        },
        {
            "nvic": "H6B05A",
            "variant": "SR TURBO KDN190R-PRPDYE 4 SP AUTOMATIC"
        },
        {
            "nvic": "H6805A",
            "variant": "LN166R 5 SP MANUAL"
        },
        {
            "nvic": "H6905A",
            "variant": "LN166R 5 SP MANUAL"
        },
        {
            "nvic": "GO205G",
            "variant": "G KUN25R-PRMSHE 5 SP MANUAL"
        },
        {
            "nvic": "GOQ05G",
            "variant": "G KUN25R-TRMDHE 5 SP MANUAL"
        }
    ]
}

Example 3 : getBasicQuotation returns with error

Request :
    {
        "vehRegNo": "WWF8325",
        "idNo": "910101-01-0101",
        "insuredType": "I",
        "insuredMaritalStatus": "S",
        "vehicleLocation": 5,
        "disabled": "N",
        "vehicleCoverage": 1,
        "vehicleCondition": "U",
        "coverFrom": "2022/02/10",
        "coverTo": "2023/02/09",
        "nvic": "",
        "agentCode": "011099-00"
    }

Response :
    {
        "statusCode": "100003",
        "statusMessage": "Invalid ID Number",
        "customerVehicleInfo": null,
        "listPlan": null,
        "nvicList": null
    }

Example 4 : getBasicQuotation with selected NVIC Code

Request: 
    {
        "vehRegNo": "WWF8325",
        "idNo": "910101-01-0101",
        "insuredType": "I",
        "insuredMaritalStatus": "S",
        "vehicleLocation": 5,
        "disabled": "N",
        "vehicleCoverage": 1,
        "vehicleCondition": "U",
        "coverFrom": "2022/02/10",
        "coverTo": "2023/02/09",
        "nvic": "4T509A”,
        "agentCode": "011099-00"
    }

Response : 
    {
	"statusCode": "000000",
	"statusMessage": "Success",
	"customerVehicleInfo": {
		"builtType": "C.B.U.",
		"capacity": "2442",
		"chassisNo": "MMBJYKL10JH038811",
		"coverType": "3",
		"engineNo": "4N15UCT9337",
		"polExpDate": "30082022",
		"preInsCode": "408",
		"vehClass": "02",
		"vehMake": "25",
		"vehModel": "28",
		"vehRegNo": "SY8144",
		"vehUse": "1",
		"yearMake": "2018",
		"curNCD": "0.3833",
		"nextNCD": "0.45",
		"ncdEffDate": "31082021",
		"ncdExpDate": "30082022",
		"nextNcdEffDate": "31082022",
		"nvicData": {
			"nvic": "JIQ18A",
			"makeCode": "25",
			"make": "MITSUBISHI",
			"modelCode": "28",
			"model": "TRITON",
			"variant": "VGT A/T PREMIUM MY16 5 SP AUTOMATIC CONVENTIONAL",
			"category": "LCV",
			"seatingCapacity": "5",
			"engineCapacity": "2442",
			"marketValue": 72900.0,
			"yearMake": "2018"
		}
	},
	"nvicList": null,
	"listPlan": [
		{
			"planCode": "Basic",
			"currency": "MYR",
			"displayName": "Option 1",
			"quotedAmount": 1175.27,
			"sumInsured": 65610.0,
			"basePremium": 1099.31,
			"detailedQuotation": {
				"coverPremium": 0.0,
				"allRiderPremium": 0.0,
				"loading": 0.0,
				"basicPremium": 1998.74,
				"ncd": 899.43,
				"extendPremium": 0.0,
				"totalExtraBenefitPremium": 0.0,
				"premium": 0.0,
				"gstDiscountAmount": 0.0,
				"gstPerc": 0.0,
				"gstAmount": 0.0,
				"stampDuty": 10.0,
				"totalNetPremium": 1065.34,
				"totalAmountPayable": 1175.27,
				"schedulePremium": 0.0,
				"discountPercents": 0.0,
				"totalDiscountAmount": 0.0,
				"premiumAfterDiscount": 1099.31,
				"trailerPremium": 0.0,
				"totalExtraCoverPremium": 0.0,
				"noClaimDiscount": 0.0,
				"totalRidersPremium": 0.0,
				"grossPremium": 1099.31,
				"grossDue": 1175.27,
				"commisionPerc": 0.1,
				"commisionAmount": 109.93,
				"totalCommision": 109.93,
				"commisionGSTPerc": 0.0,
				"commisionGSTAmount": 0.0,
				"act": 419.94,
				"excessAmount": 656.0,
				"netBNMRound": 0.0,
				"sstPerc": 0.06,
				"sstAmount": 65.96,
				"refno": "20220308183731304PM",
				"verkey": "VK00013",
				"tecH_PREM": 2989.86,
				"t_BASICPREM_SR": 2220.83,
				"annualprem": 1856.34,
				"srate": 1.0767123287671233
			},
			"listExtraBenefit": [
				{
					"extensionData": null,
					"selected": false,
					"cewCode": "119",
					"baseCEWCode": "119",
					"resultRange": null,
					"sumInsured": 0.0,
					"unitValue": 0,
					"premium": 0.0,
					"formulaCode": "U",
					"unitName": "",
					"description": "All Drivers (Non-Tariff) (RM 20.00 per vehicle)",
					"openEditFor": "",
					"defaultUnitValue": 1,
					"defaultSumInsured": 0.0,
					"category": "BASIC",
					"planName": "",
					"stampDuty": 0.0
				},
				{
					"extensionData": null,
					"selected": false,
					"cewCode": "118",
					"baseCEWCode": "118",
					"resultRange": null,
					"sumInsured": 0.0,
					"unitValue": 0,
					"premium": 0.0,
					"formulaCode": "X",
					"unitName": "",
					"description": "Private Hire Car (100% of vehicle Sum Insured)",
					"openEditFor": "",
					"defaultUnitValue": 0,
					"defaultSumInsured": 0.0,
					"category": "BASIC",
					"planName": "",
					"stampDuty": 0.0
				}
			]
		}
	]
}

```

## Update Quotation

### API Specification

|API Name|UpdateQuotation|
|---|---|
|URL Path|/motor/PostUpdateQuotation|
|Consuming System| Channel Partners|
|Feature|Select Plan, Benefits and Named Driver|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/POST|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|

#### Request Header

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|Content-Type|String|M|
|Authorization|String|M|

#### Request Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|quotationNo|String|M|
|selectedPlanCode|String|M|
|agentCode|String|M|
|additionalDriverDetails|List [[Additional Driver Model]]|O|
|listBenefits|List [[Benefits Model]]|O|

#### Response Body

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|statusCode|String|M|
|statusMessage|String|M|
|quotationNo|String|M|
|CoverFrom|Date|M|
|CoverTo|Date|M|
|policyPlan|[Plan Model]|M|
|driverDetails|List [[Additional Driver Model]]|O|

``` javascript
Example Request and Response Message

Request Header:
    {
        Content-Type: "application/json"
        Authorization: "Bearer eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJ0YW9zX3VhdEBjYXJib24uc3VwZXIiLCJhdXQiOiJBUFBMSUNBVElPTiIsImF1ZCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJuYmYiOjE2NDc0ODg1MzUsImF6cCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJzY29wZSI6ImRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvYXBpcG9ydGFsdWF0LnR1bmVwcm90ZWN0LmNvbTo0NDNcL29hdXRoMlwvdG9rZW4iLCJleHAiOjE2NDc0OTIxMzUsImlhdCI6MTY0NzQ4ODUzNSwianRpIjoiNzM4ZDU4YmUtMTk3Mi00M2YwLTk4YjItNTg1NDZlNjIzY2U5In0.dlZXiEZHwvQIeyk2JE52nEIDNZR6G54IVP5_WpfBjW57ZlUcGW1ZPT--6V5h9az5pPvwx-Sgev5sLDIHPQbem97kuOCWbc6X1Ki35_yHGHH8livXKdn9F6IOrwKrlrhu6ACV35OFDON1uZLA4ngnTGgtG1PFEErS8tm43JeOydUWj3rzfJQXqvH7DnITZO7Cle_tnkYnddwWstxVDRNFfY03fr55l6oC9BYWBq-tcBnfEvU5t0R-8_Oyb0s9PjqL_MOMSKUv9s7ppA1AeZ6A9xOUfbeAsQ7QV0KrkAvzyslCEqr-08wEkxTvADhknN0dmx_btmXMGclzcZVpQR9cPA"
    }

Example 1: Update quotation and returns sucessful response

Request:
    {
		"quotationNo": "PNQ-2022426000055",
		"selectedPlanCode": "Choice",
		"agentCode": "011099-00",
		"additionalDriverDetails": [
			{
				"seqNo": 0,
				"driverName": "Tony Stark",
				"idType": 0,
				"idNo": "700529-01-0001",
				"drivingExperience": 10,
				"occupationCode": "01*01",
				"relationshipCode": 2
			},
			{
				"seqNo": 1,
				"driverName": "Natasha Romanoff",
				"idType": 1,
				"idNo": "A12345",
				"nationality": "RUS",
				"dateOfBirth": "1984-12-03",
				"drivingExperience": 7,
				"occupationCode": "02*02",
				"relationshipCode": 2
			},
			{
				"seqNo": 2,
				"driverName": "Wanda Maximoff",
				"idType": 0,
				"idNo": "900210-03-0003",
				"drivingExperience": 30,
				"occupationCode": "01*01",
				"relationshipCode": 4
			}
		],
		"listBenefits": [
			{
				"isSelected": true,
				"cewCode": "100",
				"baseCEWCode": "100",
				"sumInsured": 1000,
				"unitValue": 1,
				"category": "BASIC"
			},
			{
				"isSelected": true,
				"cewCode": "57A",
				"baseCEWCode": "57",
				"sumInsured": 2000,
				"unitValue": 1,
				"category": "BASIC"
			},
			{
				"isSelected": true,
				"cewCode": "89",
				"baseCEWCode": "89",
				"sumInsured": 0,
				"unitValue": 0,
				"category": "BASIC"
			}
		]
	}

Response:
{
    "statusCode": "000000",
    "statusMessage": "Success",
    "quotationNo": "PNQ-2022426000056",
    "coverFrom": "2022-04-22T00:00:00",
    "coverTo": "2022-04-22T00:00:00",
    "policyPlan": {
        "planCode": "Choice",
        "currency": "MYR",
        "displayName": "Option 2",
        "quotedAmount": 365.82,
        "sumInsured": 76000.0000,
        "basePremium": 335.68,
        "detailedQuotation": {
            "coverPremium": 0.0,
            "allRiderPremium": 0.0,
            "loading": 0.00,
            "basicPremium": 257.52,
            "ncd": 141.64,
            "extendPremium": 0.0,
            "totalExtraBenefitPremium": 219.80,
            "premium": 0.0,
            "gstDiscountAmount": 0.0,
            "gstPerc": 0.0,
            "gstAmount": 0.00,
            "stampDuty": 10.0,
            "totalNetPremium": 332.25,
            "totalAmountPayable": 365.82,
            "schedulePremium": 0.0,
            "discountPercents": 0.0,
            "totalDiscountAmount": 0.0,
            "premiumAfterDiscount": 335.68,
            "trailerPremium": 0.0,
            "totalExtraCoverPremium": 0.0,
            "noClaimDiscount": 0.0,
            "totalRidersPremium": 0.0,
            "grossPremium": 335.68,
            "grossDue": 365.82,
            "commisionPerc": 0.10,
            "commisionAmount": 33.57,
            "totalCommision": 33.57,
            "commisionGSTPerc": 0.00,
            "commisionGSTAmount": 0.00,
            "act": 82.07,
            "excessAmount": 0.0,
            "netBNMRound": 0.0,
            "sstPerc": 0.06,
            "sstAmount": 20.14,
            "refno": "20220426150108366PM",
            "verkey": "VK00014",
            "tecH_PREM": 225.95,
            "t_BASICPREM_SR": 286.14,
            "annualprem": 2060.19,
            "srate": 0.125
        },
        "listExtraBenefit": [
            {
                "extensionData": null,
                "selected": true,
                "cewCode": "100",
                "baseCEWCode": "100",
                "resultRange": null,
                "sumInsured": 1000.0,
                "unitValue": 1,
                "premium": 37.80,
                "formulaCode": "Z",
                "unitName": "PSGR",
                "description": "Legal Liability to Passengers",
                "openEditFor": null,
                "defaultUnitValue": 0,
                "defaultSumInsured": 0.0,
                "category": "BASIC",
                "planName": null,
                "stampDuty": 0.0
            },
            {
                "extensionData": null,
                "selected": true,
                "cewCode": "57A",
                "baseCEWCode": "57",
                "resultRange": null,
                "sumInsured": 76000.0000,
                "unitValue": 1,
                "premium": 152.0,
                "formulaCode": "V",
                "unitName": "",
                "description": "Inclusion Of Special Perils (0.20% of Vehicle Sum Insured)",
                "openEditFor": null,
                "defaultUnitValue": 0,
                "defaultSumInsured": 0.0,
                "category": "BASIC",
                "planName": null,
                "stampDuty": 0.0
            },
            {
                "extensionData": null,
                "selected": true,
                "cewCode": "89",
                "baseCEWCode": "89",
                "resultRange": null,
                "sumInsured": 0.0,
                "unitValue": 0,
                "premium": 30.0,
                "formulaCode": "S",
                "unitName": "",
                "description": "Windscreen (15% of windscreen sum insured)",
                "openEditFor": null,
                "defaultUnitValue": 0,
                "defaultSumInsured": 0.0,
                "category": "BASIC",
                "planName": null,
                "stampDuty": 0.0
            },
            {
                "extensionData": null,
                "selected": true,
                "cewCode": "07",
                "baseCEWCode": "07",
                "resultRange": null,
                "sumInsured": 0.0,
                "unitValue": 0,
                "premium": 0.0,
                "formulaCode": "U",
                "unitName": "DRIVER",
                "description": "Additional Named Driver (RM10.00 per additional driver)",
                "openEditFor": null,
                "defaultUnitValue": 0,
                "defaultSumInsured": 0.0,
                "category": "BASIC",
                "planName": null,
                "stampDuty": 0.0
            }
        ],
        "listSelectedExtraCoverInfo": null
    },
    "driverDetails": [
        {
            "seqNo": 1,
            "driverName": "THE POLICYHOLDER",
            "idType": 0,
            "idNo": "781221-08-6237",
            "age": 44,
            "nationality": null,
            "dateOfBirth": "1978-12-21T00:00:00",
            "drivingExperience": 5.0,
            "occupationCode": "01*01",
            "relationshipCode": 0
        },
        {
            "seqNo": 2,
            "driverName": "Tony Stark",
            "idType": 0,
            "idNo": "700529-01-0001",
            "age": 52,
            "nationality": "MYS",
            "dateOfBirth": "1970-05-29T00:00:00",
            "drivingExperience": 10.0,
            "occupationCode": "01*01",
            "relationshipCode": 2
        },
        {
            "seqNo": 3,
            "driverName": "Natasha Romanoff",
            "idType": 1,
            "idNo": "A12345",
            "age": 38,
            "nationality": "RUS",
            "dateOfBirth": "1984-12-03T00:00:00",
            "drivingExperience": 7.0,
            "occupationCode": "02*02",
            "relationshipCode": 2
        },
        {
            "seqNo": 4,
            "driverName": "Wanda Maximoff",
            "idType": 0,
            "idNo": "900210-03-0003",
            "age": 32,
            "nationality": "MYS",
            "dateOfBirth": "1990-02-10T00:00:00",
            "drivingExperience": 30.0,
            "occupationCode": "01*01",
            "relationshipCode": 4
        }
    ]
}

```

## Get Basic Quotation via Quotation No

### API Specification

|API Name|Get Basic Quotation Via Quotation No|
|---|---|
|URL Path|/motor/GetBasicQuotationViaQuotationNo|
|Consuming System| Channel Partners|
|Feature|Get Previous Quotation Details via quotation number provided previously|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/GET|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|

#### Request Header

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|Content-Type|String|M|
|Authorization|String|M|

#### Request Query String

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|quotationNo|String|M|
|agentCode|String|M|
#### Response Body

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|statusCode|String|M|
|statusMessage|String|M|
|quotationNo|String|M|
|quotationStatusId|Integer|M|
|CoverFrom|Date|M|
|CoverTo|Date|M|
|customerVehicleInfo|[Vehicle Info Model]|M|
|listPlan|List [[Plan Model]]|C|
|nvicList|List [[NVIC Variant Model]]|C|

``` javascript
Example Request and Response Message

Request Header:
    {
        Content-Type: "application/json"
        Authorization: "Bearer eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJ0YW9zX3VhdEBjYXJib24uc3VwZXIiLCJhdXQiOiJBUFBMSUNBVElPTiIsImF1ZCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJuYmYiOjE2NDc0ODg1MzUsImF6cCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJzY29wZSI6ImRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvYXBpcG9ydGFsdWF0LnR1bmVwcm90ZWN0LmNvbTo0NDNcL29hdXRoMlwvdG9rZW4iLCJleHAiOjE2NDc0OTIxMzUsImlhdCI6MTY0NzQ4ODUzNSwianRpIjoiNzM4ZDU4YmUtMTk3Mi00M2YwLTk4YjItNTg1NDZlNjIzY2U5In0.dlZXiEZHwvQIeyk2JE52nEIDNZR6G54IVP5_WpfBjW57ZlUcGW1ZPT--6V5h9az5pPvwx-Sgev5sLDIHPQbem97kuOCWbc6X1Ki35_yHGHH8livXKdn9F6IOrwKrlrhu6ACV35OFDON1uZLA4ngnTGgtG1PFEErS8tm43JeOydUWj3rzfJQXqvH7DnITZO7Cle_tnkYnddwWstxVDRNFfY03fr55l6oC9BYWBq-tcBnfEvU5t0R-8_Oyb0s9PjqL_MOMSKUv9s7ppA1AeZ6A9xOUfbeAsQ7QV0KrkAvzyslCEqr-08wEkxTvADhknN0dmx_btmXMGclzcZVpQR9cPA"
    }

Example 1: Get Quotation Details via Quotation Number and returns sucessful response

Request Query String:
    {URLPath}/?quotationNo=PNQ-2022427000002&agentCode=011099-00

Response:
{
    "statusCode": "000000",
    "statusMessage": "Success",
    "quotationNo": "PNQ-2022427000002",
    "quotationStatusId": 3,
    "coverFrom": "2022-04-22T00:00:00",
    "coverTo": "2022-04-22T00:00:00",
    "customerVehicleInfo": {
        "builtType": "C.K.D.",
        "capacity": "1998",
        "chassisNo": "PP1KEA271GM101002",
        "coverType": "3",
        "engineNo": "PE10348125",
        "polExpDate": "04082022",
        "preInsCode": "247",
        "vehClass": "02",
        "vehMake": "24",
        "vehModel": "50",
        "vehRegNo": "F1785",
        "vehUse": "1",
        "yearMake": "2016",
        "curNCD": "0.55",
        "nextNCD": "0.55",
        "ncdEffDate": "05082021",
        "ncdExpDate": "04082022",
        "nextNcdEffDate": "05082022",
        "nvicData": {
            "nvic": "J7S16C",
            "makeCode": "24",
            "make": "MAZDA",
            "modelCode": "50",
            "model": "CX-5",
            "variant": "GLS 2WD (CKD) FACELIFT 6 SP AUTO ACTIVEMATIC",
            "category": "PAS",
            "seatingCapacity": "5",
            "engineCapacity": "1998",
            "marketValue": 76000.0,
            "yearMake": "2016"
        }
    },
    "listPlan": [
        {
            "planCode": "Basic",
            "currency": "MYR",
            "displayName": "Option 1",
            "quotedAmount": 123.07,
            "sumInsured": 68400.0000,
            "basePremium": 106.67,
            "detailedQuotation": {
                "coverPremium": 0.0,
                "allRiderPremium": 0.0,
                "loading": 0.00,
                "basicPremium": 237.05,
                "ncd": 130.38,
                "extendPremium": 0.0,
                "totalExtraBenefitPremium": 0.0,
                "premium": 0.0,
                "gstDiscountAmount": 0.0,
                "gstPerc": 0.0,
                "gstAmount": 0.00,
                "stampDuty": 10.0,
                "totalNetPremium": 112.40,
                "totalAmountPayable": 123.07,
                "schedulePremium": 0.0,
                "discountPercents": 0.0,
                "totalDiscountAmount": 0.0,
                "premiumAfterDiscount": 106.67,
                "trailerPremium": 0.0,
                "totalExtraCoverPremium": 0.0,
                "noClaimDiscount": 0.0,
                "totalRidersPremium": 0.0,
                "grossPremium": 106.67,
                "grossDue": 123.07,
                "commisionPerc": 0.10,
                "commisionAmount": 10.67,
                "totalCommision": 10.67,
                "commisionGSTPerc": 0.00,
                "commisionGSTAmount": 0.00,
                "act": 40.75,
                "excessAmount": 0.0,
                "netBNMRound": 0.0,
                "sstPerc": 0.06,
                "sstAmount": 6.40,
                "refno": "20220427145505715PM",
                "verkey": "VK00014",
                "tecH_PREM": 212.98,
                "t_BASICPREM_SR": 263.39,
                "annualprem": 1896.39,
                "srate": 0.125
            },
            "listExtraBenefit": [
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "119",
                    "baseCEWCode": "119",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "All Drivers (Non-Tariff) (RM 20.00 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "118",
                    "baseCEWCode": "118",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "X",
                    "unitName": "",
                    "description": "Private Hire Car (100% of vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "07",
                    "baseCEWCode": "07",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "DRIVER",
                    "description": "Additional Named Driver (RM10.00 per additional driver)",
                    "openEditFor": "Unit",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "100",
                    "baseCEWCode": "100",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "Z",
                    "unitName": "PSGR",
                    "description": "Legal Liability to Passengers",
                    "openEditFor": "Unit",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "25",
                    "baseCEWCode": "25",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Strike, Riot & Civil Commotion (0.30% of Vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "57A",
                    "baseCEWCode": "57",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils (0.20% of Vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "72",
                    "baseCEWCode": "72",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Legal Liability of Passengers For Acts Of Negligence (RM 7.50 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "89",
                    "baseCEWCode": "89",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Windscreen (15% of windscreen sum insured)",
                    "openEditFor": "SumInsured",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "97",
                    "baseCEWCode": "97",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Vehicle Accessories (15% of item value)",
                    "openEditFor": "SumInsured",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A",
                    "baseCEWCode": "TDP/A",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect – Inconvenience Allowance",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN1",
                    "baseCEWCode": "TDP/A-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM50 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN2",
                    "baseCEWCode": "TDP/A-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM100 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN3",
                    "baseCEWCode": "TDP/A-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM150 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN4",
                    "baseCEWCode": "TDP/A-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM200 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S",
                    "baseCEWCode": "TDP/S",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect - Spray Painting",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN1",
                    "baseCEWCode": "TDP/S-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM1,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN2",
                    "baseCEWCode": "TDP/S-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM2,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN3",
                    "baseCEWCode": "TDP/S-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM3,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN4",
                    "baseCEWCode": "TDP/S-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM4,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "111",
                    "baseCEWCode": "111",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Current Year NCD Relief (15% of NCD value)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "101P",
                    "baseCEWCode": "101",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To The Kingdom Of Thailand",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "102",
                    "baseCEWCode": "102",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To West Kalimantan, Indonesia",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "112",
                    "baseCEWCode": "112",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Compensation For Assessed Repair Time (CART) Extension",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "117",
                    "baseCEWCode": "117",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils on First Loss Basis - Limit: 25% of Vehicle Sum Insured",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                }
            ],
            "listSelectedExtraCoverInfo": []
        },
        {
            "planCode": "Choice",
            "currency": "MYR",
            "displayName": "Option 2",
            "quotedAmount": 365.82,
            "sumInsured": 76000.0000,
            "basePremium": 335.68,
            "detailedQuotation": {
                "coverPremium": 0.0,
                "allRiderPremium": 0.0,
                "loading": 0.00,
                "basicPremium": 257.52,
                "ncd": 141.64,
                "extendPremium": 0.0,
                "totalExtraBenefitPremium": 219.80,
                "premium": 0.0,
                "gstDiscountAmount": 0.0,
                "gstPerc": 0.0,
                "gstAmount": 0.00,
                "stampDuty": 10.0,
                "totalNetPremium": 332.25,
                "totalAmountPayable": 365.82,
                "schedulePremium": 0.0,
                "discountPercents": 0.0,
                "totalDiscountAmount": 0.0,
                "premiumAfterDiscount": 335.68,
                "trailerPremium": 0.0,
                "totalExtraCoverPremium": 0.0,
                "noClaimDiscount": 0.0,
                "totalRidersPremium": 0.0,
                "grossPremium": 335.68,
                "grossDue": 365.82,
                "commisionPerc": 0.10,
                "commisionAmount": 33.57,
                "totalCommision": 33.57,
                "commisionGSTPerc": 0.00,
                "commisionGSTAmount": 0.00,
                "act": 82.07,
                "excessAmount": 0.0,
                "netBNMRound": 0.0,
                "sstPerc": 0.06,
                "sstAmount": 20.14,
                "refno": "20220427145813987PM",
                "verkey": "VK00014",
                "tecH_PREM": 225.95,
                "t_BASICPREM_SR": 286.14,
                "annualprem": 2060.19,
                "srate": 0.125
            },
            "listExtraBenefit": [
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "119",
                    "baseCEWCode": "119",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "All Drivers (Non-Tariff) (RM 20.00 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "118",
                    "baseCEWCode": "118",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "X",
                    "unitName": "",
                    "description": "Private Hire Car (100% of vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "07",
                    "baseCEWCode": "07",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 2,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "DRIVER",
                    "description": "Additional Named Driver (RM10.00 per additional driver)",
                    "openEditFor": "Unit",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "25",
                    "baseCEWCode": "25",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Strike, Riot & Civil Commotion (0.30% of Vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "72",
                    "baseCEWCode": "72",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Legal Liability of Passengers For Acts Of Negligence (RM 7.50 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "97",
                    "baseCEWCode": "97",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Vehicle Accessories (15% of item value)",
                    "openEditFor": "SumInsured",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A",
                    "baseCEWCode": "TDP/A",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect – Inconvenience Allowance",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN1",
                    "baseCEWCode": "TDP/A-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM50 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN2",
                    "baseCEWCode": "TDP/A-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM100 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN3",
                    "baseCEWCode": "TDP/A-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM150 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN4",
                    "baseCEWCode": "TDP/A-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM200 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S",
                    "baseCEWCode": "TDP/S",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect - Spray Painting",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN1",
                    "baseCEWCode": "TDP/S-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM1,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN2",
                    "baseCEWCode": "TDP/S-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM2,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN3",
                    "baseCEWCode": "TDP/S-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM3,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN4",
                    "baseCEWCode": "TDP/S-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM4,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "111",
                    "baseCEWCode": "111",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Current Year NCD Relief (15% of NCD value)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "101P",
                    "baseCEWCode": "101",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To The Kingdom Of Thailand",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "102",
                    "baseCEWCode": "102",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To West Kalimantan, Indonesia",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "112",
                    "baseCEWCode": "112",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Compensation For Assessed Repair Time (CART) Extension",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "117",
                    "baseCEWCode": "117",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils on First Loss Basis - Limit: 25% of Vehicle Sum Insured",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": true,
                    "cewCode": "100",
                    "baseCEWCode": "100",
                    "resultRange": null,
                    "sumInsured": 1000.0,
                    "unitValue": 1,
                    "premium": 37.80,
                    "formulaCode": "Z",
                    "unitName": "PSGR",
                    "description": "Legal Liability to Passengers",
                    "openEditFor": null,
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": null,
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": true,
                    "cewCode": "57A",
                    "baseCEWCode": "57",
                    "resultRange": null,
                    "sumInsured": 76000.0000,
                    "unitValue": 1,
                    "premium": 152.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils (0.20% of Vehicle Sum Insured)",
                    "openEditFor": null,
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": null,
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": true,
                    "cewCode": "89",
                    "baseCEWCode": "89",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 30.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Windscreen (15% of windscreen sum insured)",
                    "openEditFor": null,
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": null,
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": true,
                    "cewCode": "07",
                    "baseCEWCode": "07",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "DRIVER",
                    "description": "Additional Named Driver (RM10.00 per additional driver)",
                    "openEditFor": null,
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": null,
                    "stampDuty": 0.0
                }
            ],
            "listSelectedExtraCoverInfo": null
        },
        {
            "planCode": "Plus",
            "currency": "MYR",
            "displayName": "Option 3",
            "quotedAmount": 138.42,
            "sumInsured": 79800.0000,
            "basePremium": 121.15,
            "detailedQuotation": {
                "coverPremium": 0.0,
                "allRiderPremium": 0.0,
                "loading": 0.00,
                "basicPremium": 269.22,
                "ncd": 148.07,
                "extendPremium": 0.0,
                "totalExtraBenefitPremium": 0.0,
                "premium": 0.0,
                "gstDiscountAmount": 0.0,
                "gstPerc": 0.0,
                "gstAmount": 0.00,
                "stampDuty": 10.0,
                "totalNetPremium": 126.30,
                "totalAmountPayable": 138.42,
                "schedulePremium": 0.0,
                "discountPercents": 0.0,
                "totalDiscountAmount": 0.0,
                "premiumAfterDiscount": 121.15,
                "trailerPremium": 0.0,
                "totalExtraCoverPremium": 0.0,
                "noClaimDiscount": 0.0,
                "totalRidersPremium": 0.0,
                "grossPremium": 121.15,
                "grossDue": 138.42,
                "commisionPerc": 0.10,
                "commisionAmount": 12.12,
                "totalCommision": 12.12,
                "commisionGSTPerc": 0.00,
                "commisionGSTAmount": 0.00,
                "act": 46.28,
                "excessAmount": 0.0,
                "netBNMRound": 0.0,
                "sstPerc": 0.06,
                "sstAmount": 7.27,
                "refno": "20220427145505731PM",
                "verkey": "VK00014",
                "tecH_PREM": 233.37,
                "t_BASICPREM_SR": 299.14,
                "annualprem": 2153.79,
                "srate": 0.125
            },
            "listExtraBenefit": [
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "119",
                    "baseCEWCode": "119",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "All Drivers (Non-Tariff) (RM 20.00 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "118",
                    "baseCEWCode": "118",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "X",
                    "unitName": "",
                    "description": "Private Hire Car (100% of vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "07",
                    "baseCEWCode": "07",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "DRIVER",
                    "description": "Additional Named Driver (RM10.00 per additional driver)",
                    "openEditFor": "Unit",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "100",
                    "baseCEWCode": "100",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "Z",
                    "unitName": "PSGR",
                    "description": "Legal Liability to Passengers",
                    "openEditFor": "Unit",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "25",
                    "baseCEWCode": "25",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Strike, Riot & Civil Commotion (0.30% of Vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "57A",
                    "baseCEWCode": "57",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils (0.20% of Vehicle Sum Insured)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "72",
                    "baseCEWCode": "72",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Legal Liability of Passengers For Acts Of Negligence (RM 7.50 per vehicle)",
                    "openEditFor": "",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "89",
                    "baseCEWCode": "89",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Windscreen (15% of windscreen sum insured)",
                    "openEditFor": "SumInsured",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "97",
                    "baseCEWCode": "97",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Vehicle Accessories (15% of item value)",
                    "openEditFor": "SumInsured",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A",
                    "baseCEWCode": "TDP/A",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect – Inconvenience Allowance",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN1",
                    "baseCEWCode": "TDP/A-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM50 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN2",
                    "baseCEWCode": "TDP/A-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM100 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN3",
                    "baseCEWCode": "TDP/A-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM150 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/A-PLAN4",
                    "baseCEWCode": "TDP/A-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM200 per day",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "Plan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S",
                    "baseCEWCode": "TDP/S",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "S",
                    "unitName": "",
                    "description": "Tune Drive Protect - Spray Painting",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN1",
                    "baseCEWCode": "TDP/S-PLAN1",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 1 - RM1,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN2",
                    "baseCEWCode": "TDP/S-PLAN2",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 2 - RM2,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN3",
                    "baseCEWCode": "TDP/S-PLAN3",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 3 - RM3,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "TDP/S-PLAN4",
                    "baseCEWCode": "TDP/S-PLAN4",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "U",
                    "unitName": "",
                    "description": "Plan 4 - RM4,000 Max.Limit",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 1,
                    "defaultSumInsured": 0.0,
                    "category": "PrivatePlan",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "111",
                    "baseCEWCode": "111",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Current Year NCD Relief (15% of NCD value)",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "101P",
                    "baseCEWCode": "101",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To The Kingdom Of Thailand",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "102",
                    "baseCEWCode": "102",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Extension Of Cover To West Kalimantan, Indonesia",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "112",
                    "baseCEWCode": "112",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "N",
                    "unitName": "",
                    "description": "Compensation For Assessed Repair Time (CART) Extension",
                    "openEditFor": "DropDown",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "MORE",
                    "planName": "",
                    "stampDuty": 0.0
                },
                {
                    "extensionData": null,
                    "selected": false,
                    "cewCode": "117",
                    "baseCEWCode": "117",
                    "resultRange": null,
                    "sumInsured": 0.0,
                    "unitValue": 0,
                    "premium": 0.0,
                    "formulaCode": "V",
                    "unitName": "",
                    "description": "Inclusion Of Special Perils on First Loss Basis - Limit: 25% of Vehicle Sum Insured",
                    "openEditFor": "",
                    "defaultUnitValue": 0,
                    "defaultSumInsured": 0.0,
                    "category": "BASIC",
                    "planName": "",
                    "stampDuty": 0.0
                }
            ],
            "listSelectedExtraCoverInfo": []
        }
    ],
    "nvicList": []
}

```

## Confirm Quotation

### API Specification

|API Name|Confirm Quotation|
|---|---|
|URL Path|/motor/PostConfirmQuotation|
|Consuming System| Channel Partners|
|Feature|Confirm Quotation to generate policy schedule and send policy to JPJ|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/POST|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|

#### Request Header

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|Content-Type|String|M|
|Authorization|String|M|

#### Request Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|quotationNo|String|M|
|agentCode|String|M|
|PolicyHolderInfo|[Motor Policy Holder Info Model]|M|
|VehicleExtraInfo|[Vehicle Extra Info Model]|M|
|PreviousMotorPolicyInsurer|[Previous Motor Insurer Model]|O|
|PaymentDetails|[Payment Details Model]|O|
|ReturnURL|String|M|

Return URL will be used to callback to partner to notify on JPJ Approval Status and Message.

#### Response Body

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|statusCode|String|M|
|statusMessage|String|M|
|CoverNoteNo|String|M|

``` javascript
Example Request and Response Message

Request Header:
    {
        Content-Type: "application/json"
        Authorization: "Bearer eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6Ik16WXhNbUZrT0dZd01XSTBaV05tTkRjeE5HWXdZbU00WlRBM01XSTJOREF6WkdRek5HTTBaR1JsTmpKa09ERmtaRFJpT1RGa01XRmhNelUyWkdWbE5nX1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJ0YW9zX3VhdEBjYXJib24uc3VwZXIiLCJhdXQiOiJBUFBMSUNBVElPTiIsImF1ZCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJuYmYiOjE2NDc0ODg1MzUsImF6cCI6ImJxbjFkSUM4aTFrT1RESjhHb3NmT25mZTJnQWEiLCJzY29wZSI6ImRlZmF1bHQiLCJpc3MiOiJodHRwczpcL1wvYXBpcG9ydGFsdWF0LnR1bmVwcm90ZWN0LmNvbTo0NDNcL29hdXRoMlwvdG9rZW4iLCJleHAiOjE2NDc0OTIxMzUsImlhdCI6MTY0NzQ4ODUzNSwianRpIjoiNzM4ZDU4YmUtMTk3Mi00M2YwLTk4YjItNTg1NDZlNjIzY2U5In0.dlZXiEZHwvQIeyk2JE52nEIDNZR6G54IVP5_WpfBjW57ZlUcGW1ZPT--6V5h9az5pPvwx-Sgev5sLDIHPQbem97kuOCWbc6X1Ki35_yHGHH8livXKdn9F6IOrwKrlrhu6ACV35OFDON1uZLA4ngnTGgtG1PFEErS8tm43JeOydUWj3rzfJQXqvH7DnITZO7Cle_tnkYnddwWstxVDRNFfY03fr55l6oC9BYWBq-tcBnfEvU5t0R-8_Oyb0s9PjqL_MOMSKUv9s7ppA1AeZ6A9xOUfbeAsQ7QV0KrkAvzyslCEqr-08wEkxTvADhknN0dmx_btmXMGclzcZVpQR9cPA"
    }

Example: Confirm quotation and returns successful response

Request:
    {
      "quotationNo": "PNQ-2022426000056",
      "agentCode": "011099-00",
      "policyHolderInfo": {
        "insuredTitle": 0,
        "fullName": "Test 123",
        "race": 3,
        "religion": 4,
        "isGstRegistered": false,
        "GSTRegNo":"",
        "GSTRegDate":"",
        "email": "testing123@gmail.com",
        "officeTelNo": "03-3456789",
        "mobileNo": "012-3456789",
        "homeTelNo": "03-3456789",
        "address1": "Address Line 1",
        "address2": "Address Line 2",
        "address3": "Address Line 3",
        "postCode": "30000",
        "City": "Ipoh",
        "stateCode": 10,
        "CountryCode": "MYS"
      },
      "vehicleExtraInfo": {
        "VehicleBodyCode":"37",
        "vehicleFrom": "L",
        "piam": {
          "garageCode": "1",
          "safetyCode": "06",
          "antiTheftCode": "02",
          "permittedDriversCode": "01"
        },
        "agreedValueIndicator": "Y",
        "ncdRate": 0.55,
        "HirePurchaseCompanyCode": "MBB",
        "vehiclePurchasePrice": 10000,
        "vehiclePurchaseDate": "2022-03-06",
        "excess": 0.0
      },
      "paymentDetails": {
        "transactionId": "T12345",
        "transactionStatus": "SUCCESS",
        "transactionDate": "2022-04-26",
        "bankAuthCode": "A12345",
        "paymentOptionCode": "CC",
        "bankApprovalCode": "12345"
      },
      "returnURL": "www.google.com"
    }

Response:
    {
        "statusCode": "000001",
        "statusMessage": "Cover Note Generated Success, Pending Confirmation from JPJ.",
        "coverNoteNo": "0150122TMA003151"
    }

```

## Callback

### API Specification

|API Name|Update Partner on Policy Status|
|---|---|
|Consuming System|Channel Partners|
|Feature|Notify Partners Regarding on Policy Status and JPJ Approval Status|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/POST|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|


#### Request Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|QuotationNo|String|M|
|CoverNoteNo|String|M|
|StatusId|Integer|M|
|JPJMessage|String|M|

#### Response Body

Partners please respond ("OK") after receive callback from our API to stop our callback for the callback policy.


``` javascript
Example Request and Response Message

Example 1: Callback to Partners

Request:
    {
      "quotationNo": "PNQ-2022426000056",
      "CoverNoteNo": "0150122TMA003159",
      "StatusId": 4,
      "JPJMessage": "OK"
    }

Response:    
    Response.Write("OK");    

```

## Resend Email

### API Specification

|API Name|Resend Email|
|---|---|
|URL Path|/motor/ResendEmail|
|Consuming System|Channel Partners|
|Feature|Resend Email with policy schedule, certificate and tax invoice attachment to customers|
|Domain|B2B2C API|
|Interfacing Protocol/Method|REST/POST|
|Consumption Style| Synchronous|
|Transaction Processing Style| Synchronous|


#### Request Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|CoverNoteNo|String|M|
|AgentCode|String|M|

#### Response Body

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|StatusId|String|M|
|StatusMessage|String|M|


``` javascript
Example Request and Response Message

Example: Request to resend email with success

Request Query String:
    {URLPath}?CoverNoteNo=0150122TMA003158&AgentCode=011099-00

Response:    
{
    "statusId": "000002",
    "statusMessage": "Sending Email Request Success, queing up in SMTP server."
}

```

# Appendix

## Common Structure Definition

### Policy Holder Basic Info Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|IDType|Integer|M|
|IDNo|String|M|
|DateOfBirth|Date|C|
|Gender|Integer|C|
|InsuredMaritalStatus|Integer|O|
|IsDisabled|Boolean|O|
|DrivingExperience|Integer|O|
|OccupationCode|String|M|

### Vehicle Info Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|builtType|String|M|
|capacity|String|M|
|coverType|String|M|
|engineNo|String|M|
|polExpDate|String|M|
|preInsCode|String|M|
|vehClass|String|M|
|vehMake|String|M|
|vehModel|String|M|
|vehRegNo|String|M|
|vehUse|String|M|
|yearMake|String|M|
|curNCD|String|M|
|nextNCD|String|M|
|ncdEffDate|String|M|
|ncdExpDate|String|M|
|nextNcdEffDate|String|M|
|nvicData|[NVIC Data Basic Model]|M|

### NVIC Variant Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|nvic|String|M|
|variant|String|M|

### NVIC Data Basic Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|nvic|String|M|
|makeCode|String|M|
|make|String|M|
|modelCode|String|M|
|model|String|M|
|variant|String|M|
|category|String|M|
|seatingCapacity|String|M|
|engineCapacity|String|M|
|marketValue|Double|M|
|yearMake|String|M|

### Plan Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|planCode|String|M|
|currency|String|M|
|displayName|String|M|
|quotedAmount|Decimal|M|
|sumInsured|Decimal|M|
|basePremium|Decimal|M|
|detailedQuotation|[Basic Premium Result Model]|M|
|listExtraBenefit|List [[Extra Benefit Result Model]]|M|
|ListSelectedExtraCoverInfo|List [[Extra Cover Model]]|M|

### Basic Premium Result Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|coverPremium|Decimal|M|
|allRiderPremium|Decimal|M|
|loading|Decimal|M|
|basicPremium|Decimal|M|
|ncd|Decimal|M|
|extendPremium|Decimal|M|
|totalExtraBenefitPremium|Decimal|M|
|premium|Decimal|M|
|gstDiscountAmount|Decimal|M|
|gstPerc|Decimal|M|
|gstAmount|Decimal|M|
|stampDuty|Decimal|M|
|totalNetPremium|Decimal|M|
|totalAmountPayable|Decimal|M|
|schedulePremium|Decimal|M|
|discountPercents|Decimal|M|
|totalDiscountAmount|Decimal|M|
|PremiumAfterDiscount|Decimal|M|
|trailerPremium|Decimal|M|
|totalExtraCoverPremium|Decimal|M|
|noClaimDiscount|Decimal|M|
|totalRidersPremium|Decimal|M|
|grossPremium|Decimal|M|
|grossDue|Decimal|M|
|commissionPerc|Decimal|M|
|commissionAmount|Decimal|M|
|totalCommission|Decimal|M|
|commissionGSTPerc|Decimal|M|
|commissionGSTAmount|Decimal|M|
|act|Decimal|M|
|excessAmount|Decimal|M|
|netBNMRound|Decimal|M|
|sstPerc|Decimal|M|
|sstAmount|Decimal|M|
|refNo|String|M|
|verkey|Decimal|M|
|tecH_PREM|Decimal|M|
|t_BASICPREM_SR|Decimal|M|
|annualPrem|Decimal|M|
|srate|Decimal|M|


### Extra Benefit Result Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|extensionData|||
|selected|Boolean|M|
|cewCode|String|M|
|baseCEWCode|String|M|
|resultRange|String|M|
|sumInsured|Decimal|M|
|unitValue|Integer|M|
|premium|Decimal|M|
|formulaCode|String|M|
|unitName|String|M|
|description|String|M|
|openEditFor|String||
|defaultUnitValue|Integer|M|
|defaultSumInsured|Decimal|M|
|category|String|M|
|planName|String|M|
|stampDuty|Decimal|M|


### Extra Cover Model

|Parameter Name|Data Type|M/O/C|
|--------------|---------|-----|
|extensionData|||
|cewCode|String|M|
|CommissionPerc|Decimal|M|
|CommissionAmt|Decimal|M|
|GSTCommissionPerc|Decimal|M|
|GSTCommissionAmt|Decimal|M|
|StampDuty|Decimal|M|
|ServiceTaxPerc|Decimal|M|
|ServiceTaxAmt|Decimal|M|
|DiscPerc|Decimal|M|
|DiscAmt|Decimal|M|
|TotalBasicPremium|Decimal|M|
|TotalPayablePremium|Decimal|M|
|TotalDue|Decimal|M|
|TariffPremium|Decimal|M|
|TariffLoading|Decimal|M|
|LoadingPerc|Decimal|M|
|LoadingAmt|Decimal|M|
|GrossPremium|Decimal|M|
|NetPremium|Decimal|M|
|GrossDue|Decimal|M|
|NetDue|Decimal|M|
|SSTPERC|Decimal|M|
|SSTAMT|Decimal|M|
|Rate|Decimal|M|


### Motor Policy Holder Info Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|InsuredTitle|Integer|O|
|FullName|String|M|
|Race|Integer|M|
|Religion|Integer|O|
|isGSTRegistered|Boolean|M|
|GSTRegNo|String|C|
|GSTRegDate|Date|C|
|Email|String|O|
|OfficeTelNo|String|O|
|MobileNo|String|M|
|HomeTelNo|String|O|
|Address1|String|M|
|Address2|String|O|
|Address3|String|O|
|PostCode|String|M|
|City|String|M|
|StateCode|Integer|M|
|CountryCode|String|M|


### Vehicle Extra Info Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|VehicleBodyCode|String|O|
|VehicleFrom|String|O|
|PIAM|[PIAM Info Model]|M|
|AgreedValueIndicator|String|M|
|NCDRate|Decimal|M|
|HirePurchaseCompanyCode|String|O|
|VehiclePurchasePrice|Decimal|O|
|VehiclePurchaseDate|Date|O|
|Excess|Decimal|O|


### PIAM Info Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|GarageCode|String|M|
|SafetyCode|String|M|
|AntiTheftCode|String|M|
|PermittedDriversCode|String|M|


### Previous Motor Insurer Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|Code|String|O|
|Name|String|O|
|VehicleRegistrationNo|String|O|
|VehiclePolicyNo|String|O|
|NCDRate|Decimal|O|
|PolicyEffectiveDate|Date|O|
|PolicyExpiryDate|Date|O|


### Payment Details Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|TransactionId|String|M|
|TransactionStatus|String|M|
|TransactionDate|Date|M|
|BankAuthCode|String|M|
|PaymentOptionCode|String|M|
|BankApprovalCode|String|M|


### Additional Driver Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|seqNo|Integer|M|
|driverName|String|M|
|idType|Integer|M|
|idNo|String|M|
|age|Integer|C|
|nationality|String|C|
|dateOfBirth|Date|C|
|drivingExperience|Decimal|M|
|occupationCode|String|M|
|relationshipCode|Integer|M|


### Benefits Model

|Parameter Name|Data Type| M/O/C|
|--------------|---------|------|
|isSelected|Boolean|M|
|cewCode|String|C|
|baseCEWCode|String|C|
|sumInsured|Decimal|O|
|unitValue|Decimal|O|
|category|String|C|


## Error Codes from B2B2C API

Refer to the following for complete error codes:

|Error Code|Error Message|
|----------|-------------|
|000000|Success|
|000001|Cover Note Generated Success, Pending Confirmation from JPJ.|
|000002|Sending Email Request Success, queing up in SMTP server.|
|100001|Exceeded renew policy expiry date|
|100002|Policy cannot renew more than 60 days|
|100003|Invalid ID number|
|100004|No relate NVIC info found from ISM ABI DB|
|100005|Data not found|
|100006|NCD Lookup Failure|
|100007|Error in vehicle registration number not matches|
|100008|Vehicle age more than 20 years|
|100009|NCD Confirmation is unavailable as confirmation has been taken earlier|
|200001|ChassisNo is Empty|
|200002|Invalid vehicle registration number|
|200003|No NVIC from ISM VIX|
|200004|ISM VIX result is null|
|300001|Pricing engine calculation failed|
|400001|vehicleType is empty|
|400002|coverType is empty|
|400003|vehClass is empty|
|400004|idNo cannot be empty|
|400005|vehicleNo cannot be empty|
|400006|vehUse cannot be empty|
|400007|userID cannot be empty|
|400008|Invalid location. Valid value are E/W/EAST/WEST|
|400009|Response Code:|
|400010|Unexpected error|
|400011|Please select a NVIC code|
|400012|Invalid vehicle coverage value|
|400013|Invalid insured type value|
|400014|Invalid insured marital status|
|400015|Invalid insured disability status value|
|400016|Invalid insured vehicle condition value|
|400017|Invalid vehicle sum insured value|
|400018|Invalid NVIC selection|
|400019|Gender cannot be empty|
|400020|Date of birth cannot be empty|
|400021|Database error generating quotation number.|
|400022|Database update failed.|
|400023|Database no record found.|
|400024|No active user found.|
|400025|Occupation cannot be empty.|
|400026|Invalid benefit code selected.|
|400027|Insured Name cannot be empty.|
|400028|Insured Address cannot be empty.|
|400029|Insured Postal Code cannot be empty.|
|400030|Insured Mobile Phone Number cannot be empty.|
|400031|Submit Motor Policy failure.|
|400032|Please select either named driver extra benefits or all drivers extra benefits only.|
|400033|Vehicle Class GST Code Missing!|
|400034|Vehicle Class SST Code Missing!|
|400035|Vehicle Class Missing!|
|400036|Quotation status invalid|
|400037|Quotation Number cannot be empty or null!|
|400038|Agent Code cannot be empty or null!|
|400039|Quotation not found in DB.|
|400040|Motor Policy not found in DB.|
|400041|Receiver Mail Address cannot be empty or blank!|
|400042|Email sending failed.|
|400043|Non NRIC Named Driver Required Nation Info.|
|400044|Non NRIC Named Driver Required Age Info.|
|400045|Non NRIC Named Driver Required ID Info.|
|400046|Non NRIC Named Driver Required Birth Date Info.|
|400047|Named Driver driving experience beyond age of driver.|
|400048|Policy Holder driving experience beyond age of driver.|
|400049|Nation cannot be empty.|
|400050|Invalid ID Type Selection.|
|400051|Invalid Occupation Code Selected.|
|400052|Invalid Gender Selected.|
|400053|The age of each driver should be between 16 and 99 years old.|
|400054|Invalid Driver Nation Code selected.|
|400055|Invalid Driver ID Type selected.|
|400056|Invalid Plan Code selected.|
|400057|Invalid State Code selected.|
|400058|Invalid Driver Relationship Code selected.|
|400059|Policy Holder Driver info will be populated by API!|
|400060|Invalid Nation Code selected.|
|400061|Invalid PIAM Garage Code selected.|
|400062|Invalid PIAM Safety Features Code selected.|
|400063|Invalid PIAM Anti Theft Code selected.|
|400064|Invalid PIAM Permitted Driver Code selected.|
|400065|CBC Breached, please proceed to Aging Analysis for more detail.|
|400066|Underwriting returned status as Referred|
|400067|Underwriting returned status as Declined|
|500001|Failure in AMLA Sanctions Screening.|
|500002|AML Verification failed, policy holder blacklisted.|

## Key Value Mapping

### Quotation Status

|Code|Description|
|----|-----------|
|1|Pending|
|2|Processing|
|3|Confirmed|
|4|Issued|
|5|Failed|

### Proposer

|Code|Description|
|----|-----------|
|I|Individual|
|C|Corporate|

### State

|Code|Description|
|----|-----------|
|1|JOHOR DARUL TA'ZIM|
|2|KEDAH DARUL AMAN|
|3|KELANTAN DARUL NAIM|
|4|MELAKA|
|5|NEGERI SEMBILAN DARUL KHUSUS|
|6|PAHANG DARUL MAKMUR|
|7|PERLIS INDERA KAYANGAN|
|8|PERAK DARUL RIDZUAN|
|9|PULAU PINANG|
|10|SABAH|
|11|SARAWAK|
|12|SELANGOR DARUL EHSAN|
|13|TERENGGANU DARUL IMAN|
|14|WILAYAH PERSEKUTUAN|
|15|WILAYAH PERSEKUTUAN LABUAN|

### Title

|Code|Description|
|----|-----------|
|0|Mr|
|1|Ms|
|2|Dr|
|3|Dato'|
|4|Datin|

### Place Of Use

|Code|Description|
|----|-----------|
|E|East|
|W|West|

### Gender

|Code|Description|
|----|-----------|
|0|Male|
|1|Female|
|2|Company|
|3|Other|

### Race

|Code|Description|
|----|-----------|
|0|Malay|
|1|Chinese|
|2|Indian|
|3|Others|

### Religion

|Code|Description|
|----|------------|
|0|Muslim|
|1|Buddhist|
|2|Christian|
|3|Hindu|
|4|Other|

### Vehicle Coverage

|Code|Description|
|----|-----------|
|1|Comprehensive|
|2|Third Party|
|3|Third Party Fire & Theft|

### Vehicle Condition

|Code|Description|
|----|-----------|
|0|Motor Trade License - New Registration|
|6|Motor Trade License - Registered|
|7|International Circuit Permit (ICP)|
|8|Foreign Commercial Vehicle License|
|N|New Registration|
|T|Registered - Transfer Owner|
|U|Registered|

### Relationship Code

|Code|Description|
|----|-----------|
|0|Self|
|1|Parent And Parent-In-Law|
|2|Spouse|
|3|Child|
|4|Relative|
|5|Friend and Co-Worker|

### ID Type

|Code|Description|
|----|-----------|
|0|NRIC|
|1|Old IC|
|2|Passport No|
|3|New Business Registration No|
|4|Old Business Registration No|
|5|Other|

### Insured Type

|Code|Description|
|----|-----------|
|0|Individual|
|1|Corporate|

### Insured Marital Status

|Code|Description|
|----|-----------|
|0|Single|
|1|Married|
|2|Divorced|
|3|Others|

### Garage Code

|Code|Description|
|----|-----------|
|0|Locked Garage|
|1|Unlocked Garage|
|2|Locked Compound|
|3|Unlocked Compound|
|4|Open Public Car Park|
|5|Secure Public Car Park|
|6|Public Road|

### Safety Code

|Code|Description|
|----|-----------|
|01|DRIVERS SIDE AIRBAGS (1)|
|02|DRIVER & PASSENGER AIRBAGS (2)|
|03|AIRBAGS (MORE THEN 2)|
|04|ABS (NO AIRBAGS)|
|05|ABS & AIRBAGS 1|
|06|ABS & AIRBAGS 2|
|07|ABS & AIRBAGS (MORE THAN 2)|
|99|NONE|

### Anti Theft Code

|Code|Description|
|----|-----------|
|00|NO ALARM (WITHOUT MECHANICAL DEVICE)|
|01|ALARM, IMMOBILISER & GLOBAL (WITHOUT MECHANICAL DEVICE)|
|02|ALARM & IMMOBILIZER (WITHOUT MECHANICAL DEVICE)|
|03|FACTORY FITTED ALARM (WITHOUT MECHANICAL DEVICE)|
|09|OTHERS (WITHOUT MECHANICAL DEVICE)|
|10|NO ALARM (WITH MECHANICAL DEVICE)|
|11|ALARM, IMMOBILISER & GLOBAL (WITH MECHANICAL DEVICE)|
|12|ALARM & IMMOBILIZER (WITH MECHANICAL DEVICE)|
|13|FACTORY FITTED ALARM (WITH MECHANICAL DEVICE)|
|19|OTHERS (WITH MECHANICAL DEVICE)|

#### Permitted Driver

#### Private Car
|Coverage|Code|Description|
|----|----|-----------|
|1|00|Insured Not Driving|
|1|01|The Policy Holder|
|1|99|Any Authorised Drivers|
|2|99|Any Authorised Drivers|
|3|99|Any Authorised Drivers|

#### Motorcyclist
|Coverage|Code|Description|
|----|----|-----------|
|1|02|All Riders|
|2|99|Any Authorised Riders|
|3|99|Any Authorised Riders|


## Benefits

### Private Car Eligible

|Benefit Description|Unit Value|Sum Insured|
|---|---|---|
|All Drivers (Non-Tariff) (RM 20.00 per vehicle)|NA|NA|
|Private Hire Car (100% of vehicle Sum Insured)|NA|NA|
|Additional Named Driver (RM10.00 per additional driver)|2|NA|
|Legal Liability to Passengers|1|NA|
|Strike, Riot & Civil Commotion (0.30% of Vehicle Sum Insured)|NA|NA|
|Inclusion Of Special Perils (0.20% of Vehicle Sum Insured)|NA|NA|
|Legal Liability of Passengers For Acts Of Negligence (RM 7.50 per vehicle)|NA|NA|
|Windscreen (15% of windscreen sum insured)|NA|500/1000/2000/3000|
|Current Year NCD Relief (15% of NCD value)|NA|NA|
|Vehicle Accessories (15% of item value)|NA|500/1000/2000/3000|
|Extension Of Cover To The Kingdom Of Thailand|NA|NA|
|Extension Of Cover To West Kalimantan, Indonesia|NA|NA|
|Inclusion Of Special Perils on First Loss Basis - Limit: 25% of Vehicle Sum Insured|NA|NA|

#### Drop Down List
|Benefit Group|Benefit Description|Sample Value|
|---|---|---|
|TDP/S|Tune Drive Protect - Spray Painting|TDP/S-PLAN1, TDP/S-PLAN2, TDP/S-PLAN3|
|TDP/A|Tune Drive Protect – Inconvenience Allowance|TDP/A-PLAN1, TDP/A-PLAN2, TDP/A-PLAN3|

### Motorcyclist Eligible

|Benefit Description|Unit Value|Sum Insured|
|---|---|---|
|Passenger Liability Cover (RM15.00)|NA|NA|
|Strike, Riot & Civil Commotion (0.30% of Vehicle Sum Insured)|NA|NA|
|Inclusion Of Special Perils (RM 10.00 flat rate)|NA|NA|
|Extension Of Cover To The Kingdom Of Thailand|NA|NA|
|Extension Of Cover To West Kalimantan, Indonesia|NA|NA|


## Underwriting Rules

### Referred Vehicle
|Make|Model|
|---|---|
|BENTLEY|CONTINENTAL|
|BMW|Z4|
|MITSUBISHI|L200|
|MITSUBISHI|STORM|
|MITSUBISHI|TRITON|
|PORSCHE|911|
|PORSCHE|BOXSTER|
|PORSCHE|CAYENNE|
|PORSCHE|CAYMAN|
|PORSCHE|MACAN|
|PORSCHE|PANAMERA|
|PORSCHE|TAYCAN|
|NISSAN|FAIRLADY|
|NISSAN|GT-R|
|NISSAN|SKYLINE|
|Toyota|86|
|Toyota|Hilux|